#ifndef SIO_NSP_H
#define SIO_NSP_H

#include <QObject>
#include <QJSValue>
#include <QVariant>
#include <QMap>

namespace SocketIO {

class Client;
class Packet;

class Nsp : public QObject
{
    Q_OBJECT

    friend class Client;
public:
    explicit Nsp(QString name,Client *parent);

    Q_INVOKABLE void on(QString ev, QJSValue cb);
    Q_INVOKABLE void off(QString ev);
    Q_INVOKABLE void once(QString ev, QJSValue cb);
    Q_INVOKABLE void eemit(QString ev, QJSValue data = QJSValue(QJSValue::UndefinedValue), QJSValue cb = QJSValue(QJSValue::UndefinedValue));

signals:

private:
    void inPacket(Packet * p);
    void callEvent(QString ev, QVariant data = QVariant(QVariant::Invalid));
    void callEvent(Packet * p);
    void callAck(Packet * p);
private:
    Client * _client;
    QString _name;
    QMap<QString,QJSValue> _ioOnMap;
    QMap<QString,QJSValue> _ioOnceMap;
    QMap<int,QJSValue> _ioAcksMap;

};

}
#endif // NSP_H
