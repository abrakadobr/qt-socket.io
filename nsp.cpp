#include "nsp.h"
#include "client.h"
#include "packet.h"

#include <QJSEngine>
#include <QDebug>

namespace SocketIO {

Nsp::Nsp(QString name, Client *parent) :
    QObject(parent),
    _client(parent),
    _name(name)
{

}

void Nsp::on(QString ev, QJSValue cb)
{
    if (!cb.isCallable())
        return;
    _ioOnMap.insertMulti(ev,cb);
}

void Nsp::off(QString ev)
{
    _ioOnMap.remove(ev);
    _ioOnceMap.remove(ev);
}

void Nsp::once(QString ev, QJSValue cb)
{
    if (!cb.isCallable())
        return;
    _ioOnceMap.insertMulti(ev,cb);
}

void Nsp::eemit(QString ev, QJSValue data, QJSValue cb)
{
    QJSValue rdata = data;
    QJSValue rcb = cb;

    if (cb.isUndefined()&&data.isCallable())
    {
        rdata = cb;
        rcb = data;
    }

    Packet * p = new Packet(FrameType::Message,_client);
    p->setType(PacketType::Event);
    if (!rdata.isUndefined())
        p->setEventData(ev,rdata.toVariant());
    else
        p->setEventData(ev);
    if (rcb.isCallable())
    {
        //ack
        int ack = p->toAck();
//        qDebug() << "ACK CALL " << ack << " " << ev;
        _ioAcksMap.insert(ack,rcb);
    }
    _client->sendPacket(p);
}

void Nsp::inPacket(Packet *p)
{
//    qDebug() << "NSP IN << " << p->nsp() << " - " << p->rawData();
    if (p->frameType()==FrameType::Open)
    {
    }
    if (p->frameType()==FrameType::Close)
    {

    }
    if (p->frameType()==FrameType::Ping)
        callEvent("ping");
    if (p->frameType()==FrameType::Pong)
        callEvent("ping");
    if (p->frameType()==FrameType::Message)
    {
        if (p->type()==PacketType::Connect)
            callEvent("connected",_name);
        if (p->type()==PacketType::Disconnect)
            callEvent("disconnected",_name);
        if (p->type()==PacketType::Event)
            callEvent(p);
        if (p->type()==PacketType::Ack)
            callAck(p);
    }
}

void Nsp::callEvent(QString ev, QVariant data)
{
//    qDebug() << "call event:: " << ev;
    foreach(QJSValue cb, _ioOnceMap.values(ev))
    {
        QJSValueList args;
        QVariantList lst;
        if (data.type()!=QVariant::Invalid)
        {
            if (data.type()==QVariant::List)
                lst = data.toList();
            else
                lst << data;
        }
        foreach(QVariant v,lst)
            args << cb.engine()->toScriptValue(v);
        cb.call(args);
    }
    _ioOnceMap.remove(ev);
    foreach(QJSValue cb, _ioOnMap.values(ev))
    {
        QJSValueList args;
        QVariantList lst;
        if (data.type()!=QVariant::Invalid)
        {
            if (data.type()==QVariant::List)
                lst = data.toList();
            else
                lst << data;
        }
        foreach(QVariant v,lst)
            args << cb.engine()->toScriptValue(v);
        cb.call(args);
    }
}

void Nsp::callEvent(Packet *p)
{
    IOEvent ev = p->eventData();
    callEvent(ev.code,ev.data);
}

void Nsp::callAck(Packet *p)
{
//    qDebug() << "wow! ack type packet!! " << p->ack() << " >> " << p->rawData();
    QJSValue ackCb = _ioAcksMap.value(p->ack(),QJSValue(QJSValue::UndefinedValue));
    _ioAcksMap.remove(p->ack());
    if (!ackCb.isCallable())
        return;
    QVariantList rpl = p->ackData().toList();
    QJSValueList args;
    foreach(QVariant arg, rpl)
    {
        QJSValue data = ackCb.engine()->toScriptValue(arg);
        args << data;
    }
//    qDebug() << "ack arg " << rpl;
    ackCb.call(args);
}


}
